package com.todo

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.todo.db.table.Todos
import io.ktor.features.*
import io.ktor.http.*
import kotlin.test.*
import io.ktor.server.testing.*
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

class ApplicationTest {

    //Jsonを解析してGetResponseDataにデータを入れてreturnする
    private fun separeteJsonData(text: String): GetResponseData {
        var err_cd = 0
        var err_msg = ""
        var id: Long = 0
        var title = ""
        var detail: String? = ""
        var date: String?
        val list = mutableListOf<ToDo>()
        val todoList: List<ToDo> = list
        val conma = Regex(",\n|\n,")

        //titleやdetailに"や,があっても大丈夫なようにパースする
        val jsonText = text.replace("{", "")
            .replace("}", "")
            .replace("[", "")
            .replace("]", "")
            .trim()

        val lines = jsonText.split(conma)
        for(line in lines) {
            val block = line.trim().split(":")

            when(block[0].getPropertyData()) {
                "error_code" -> {
                    err_cd = block[1].trim().toInt()
                }
                "error_message" -> {
                    err_msg = block[1].trim().replace("\"", "")
                }
                "id" -> {
                    id = block[1].trim().toLong()
                }
                "title" -> {
                    title = block[1].getPropertyData()
                }
                "detail" -> {
                    detail = block[1].getPropertyData()
                }
                "date" -> {
                    date = block[1].getPropertyData()
                    list.add(ToDo(id, title, detail, date))
                }
            }
        }
        return GetResponseData(JsonErrorResponse(err_cd, err_msg), todoList)
    }

    // "XXXXXX"の一番前と後ろの"を取り除く また、二重符に囲まれた"は\"と認識されるので\を除外する
    private fun String.getPropertyData(): String = this.trim().substring(1, this.length - 2).replace("\\","")

    @Test
    //GET成功ケース
    fun testGetSuccess() {

        connectDB()

        transaction {
            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Get, "/todos") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.OK, response.status())

                    //リクエストをJsonレスポンスと一覧に分ける
                    val resData = separeteJsonData(response.content.toString())

                    //Jsonレスポンスチェック
                    assertEquals(JsonErrorResponse(0, ""), resData.jsonData)

                    //取得した一覧の内容と順番チェック
                    val dateComparator: Comparator<ToDo> = compareBy { it.date }
                    val list = Todos.selectAll().map {
                        ToDo(
                            id = it[Todos.id],
                            title = it[Todos.title].toString(),
                            detail = it[Todos.detail]?.toString(),
                            date = it[Todos.date]?.toDateString()
                        )
                    }.sortedWith(dateComparator)

                    //取得した一覧の要素数チェック
                    assertEquals(list.size, resData.todoList.size)

                    //それぞれの要素と日付ソートされているかチェック
                    for(i in 0 .. (list.size - 1)) {
                        assertEquals(list[i].id, resData.todoList[i].id)
                        assertEquals(list[i].title, resData.todoList[i].title)

                        //双方ともnullのときは比較しない
                        if(list[i].detail != null && resData.todoList[i].detail != null) {
                            assertEquals(list[i].detail, resData.todoList[i].detail)
                        }

                        //双方ともnullのときは比較しない
                        if(list[i].date != null && resData.todoList[i].date != null) {
                            assertEquals(list[i].date, resData.todoList[i].date)
                        }
                    }
                }
            }
        }
    }

    @Test
    //GET失敗ケース　todosテーブルを削除(drop)後に実行する
    fun testGetFail() {

        connectDB()

        transaction {
            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Get, "/todos") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.InternalServerError, response.status())

                    //Jsonレスポンスチェック
                    val jsonRes = jacksonObjectMapper().readValue<JsonErrorResponse>(response.content.toString())
                    assertEquals(JsonErrorResponse(3, "一覧の取得に失敗しました"), jsonRes)
                }
            }
        }
    }

    @Test
    //POST成功ケース
    fun testPostSuccess() {
        val title = "PostTest date is null"
        val detail: String? = null
        val date: String? = null
        val reqData = ToDoCreateParameter(title, detail, date)

        connectDB()

        transaction {
            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Post, "/todos") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                    setBody(jacksonObjectMapper().writeValueAsString(reqData))
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.OK, response.status())

                    //Jsonレスポンスチェック
                    val jsonRes = jacksonObjectMapper().readValue<JsonErrorResponse>(response.content.toString())
                    assertEquals(JsonErrorResponse(0, ""), jsonRes)

                    //指定した内容通りに登録されているかチェック
                    val todo = Todos.select { Todos.title eq title }.firstOrNull() ?: throw NotFoundException()
                    assertEquals(title, todo[Todos.title])
                    assertEquals(detail, todo[Todos.detail])
                    assertEquals(date, todo[Todos.date]?.toDateString())
                }
            }
        }
    }

    @Test
    //POST失敗ケース　リクエスト形式の不正
    fun testPostFail1() {
        val title = "PostTest2"
        val detail = "PostTest2"
        val date = "test"
        val reqData = ToDoCreateParameter(title, detail, date)

        connectDB()

        transaction {
            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Post, "/todos") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                    setBody(jacksonObjectMapper().writeValueAsString(reqData))
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.BadRequest, response.status())

                    //Jsonレスポンスチェック
                    val jsonRes = jacksonObjectMapper().readValue<JsonErrorResponse>(response.content.toString())
                    assertEquals(JsonErrorResponse(2, "リクエストの形式が不正です"), jsonRes)

                    //登録されていないかチェック
                    val todo = Todos.select { Todos.title eq title }.firstOrNull()
                    assertEquals(null, todo)
                }
            }
        }
    }

    @Test
    //POST失敗ケース　登録失敗　todosテーブルを削除(drop)後に実行する
    fun testPostFail2() {
        val title = "PostTest3"
        val detail = "PostTest3"
        val date = "2022-03-01"
        val reqData = ToDoCreateParameter(title, detail, date)

        connectDB()

        transaction {
            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Post, "/todos") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                    setBody(jacksonObjectMapper().writeValueAsString(reqData))
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.InternalServerError, response.status())

                    //Jsonレスポンスチェック
                    val jsonRes = jacksonObjectMapper().readValue<JsonErrorResponse>(response.content.toString())
                    assertEquals(JsonErrorResponse(4, "登録に失敗しました"), jsonRes)
                }
            }
        }
    }

    @Test
    //PUT成功ケース
    fun testPutSuccess() {
        val id: Long = 12
        val title = "PutTest1"
        val detail = "Put_Test1"
        val date = "2022-03-01"

        val reqData = ToDoCreateParameter(title, detail, date)

        connectDB()

        transaction {
            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Put, "/todos/${id}") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                    setBody(jacksonObjectMapper().writeValueAsString(reqData))
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.OK, response.status())

                    //Jsonレスポンスチェック
                    val jsonRes = jacksonObjectMapper().readValue<JsonErrorResponse>(response.content.toString())
                    assertEquals(JsonErrorResponse(0, ""), jsonRes)

                    //指定した内容通りに更新されているかをチェック
                    val todo = Todos.select { Todos.id eq id }.firstOrNull() ?: throw NotFoundException()

                    assertEquals(title, todo[Todos.title])
                    assertEquals(detail, todo[Todos.detail])
                    assertEquals(date, todo[Todos.date]?.toDateString())
                }
            }
        }
    }

    @Test
    //PUT失敗ケース　リクエスト形式の不正
    fun testPutFail1() {
        val id: Long = 12
        val title = "PutTest2"
        val detail = "Put_Test2"
        val date = "test"

        val reqData = ToDoCreateParameter(title, detail, date)
        var todo_pre: ResultRow? = null

        connectDB()

        transaction {
            //更新前比較用に取っておく
            todo_pre = Todos.select { Todos.id eq id }.firstOrNull()

            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Put, "/todos/${id}") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                    setBody(jacksonObjectMapper().writeValueAsString(reqData))
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.BadRequest, response.status())

                    //Jsonレスポンスチェック
                    val jsonRes = jacksonObjectMapper().readValue<JsonErrorResponse>(response.content.toString())
                    assertEquals(JsonErrorResponse(2, "リクエストの形式が不正です"), jsonRes)
                }
            }
        }

        transaction {
            //更新されていないかを確認する
            val todo = Todos.select { Todos.id eq id }.firstOrNull()

            assertEquals(todo_pre?.get(Todos.title), todo?.get(Todos.title))
            assertEquals(todo_pre?.get(Todos.detail), todo?.get(Todos.detail))
            assertEquals(todo_pre?.get(Todos.date), todo?.get(Todos.date))
        }
    }

    @Test
    //PUT失敗ケース　それ以外のエラー　今回は存在しないIDを与える
    fun testPutFail2() {
        val id: Long = 20
        val title = "PutTest3"
        val detail = "Put_Test3"
        val date = "2022-03-01"

        val reqData = ToDoCreateParameter(title, detail, date)

        connectDB()

        transaction {
            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Put, "/todos/${id}") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                    setBody(jacksonObjectMapper().writeValueAsString(reqData))
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.InternalServerError, response.status())

                    //Jsonレスポンスチェック
                    val jsonRes = jacksonObjectMapper().readValue<JsonErrorResponse>(response.content.toString())
                    assertEquals(JsonErrorResponse(5, "更新に失敗しました"), jsonRes)
                }
            }
        }

        transaction {
            //指定されたidが存在しないことを確認する
            val todo = Todos.select { Todos.id eq id }.firstOrNull()
            assertEquals(null, todo)
        }
    }

    @Test
    //DELETE成功ケース
    fun testDeleteSuccess() {
        val id: Long = 1

        connectDB()

        transaction {
            //更新前比較用に取っておく
            val todo_pre = Todos.select { Todos.id eq id }.firstOrNull()

            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Delete, "/todos/${id}") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.OK, response.status())

                    //Jsonレスポンスチェック
                    val jsonRes = jacksonObjectMapper().readValue<JsonErrorResponse>(response.content.toString())
                    assertEquals(JsonErrorResponse(0, ""), jsonRes)

                    //指定されたidが元々存在していたか
                    assertNotNull(todo_pre)
                }
            }

        }
        transaction {
            //DELETEリクエスト後、指定したものは消えているか
            val todo = Todos.select { Todos.id eq id }.firstOrNull()
            assertEquals(null, todo)
        }
    }

    @Test
    //DELETE失敗ケース　今回は存在しないidを与える
    fun testDeleteFail() {
        val id: Long = 3

        connectDB()

        transaction {
            withTestApplication({ module(testing = true) }) {
                handleRequest(HttpMethod.Delete, "/todos/${id}") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                }.run {
                    //HTTPステータスチェック
                    assertEquals(HttpStatusCode.InternalServerError, response.status())

                    //Jsonレスポンスチェック
                    val jsonRes = jacksonObjectMapper().readValue<JsonErrorResponse>(response.content.toString())
                    assertEquals(JsonErrorResponse(6, "削除に失敗しました"), jsonRes)

                    //指定されたidが存在していないことをチェック
                    val todo = Todos.select { Todos.id eq id }.firstOrNull()
                    assertEquals(null, todo)
                }
            }
        }
    }
}

package com.todo

import org.joda.time.DateTime

data class ToDo(
    val id: Long,
    val title: String,
    val detail: String?,
    val date: String?
)

// "YYYY-MM-DD"フォーマットになっているかチェックする拡張関数
// 正確にはMySQLのDateTime型は　1000-01-01 ~ 9999-12-31 の範囲である必要があるのでそれをチェック
fun String.checkDateFormat(): Boolean {
    val format = Regex("[1-9][0-9]{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])")
    return format.containsMatchIn(this)
}

// "YYYY-MM-DD"をDateTime型にする拡張関数
fun String.toDateTime(): DateTime? {
    val block = this.replace("\"", "").split("-")
    return DateTime(
        block[0].toInt(),
        block[1].toInt(),
        block[2].toInt(),
        0,
        0
    )
}

// DateTime型を"YYYY-MM-DD"形式に出力する拡張関数
fun DateTime.toDateString(): String {
    val year = this.year
    val month = this.monthOfYear.toString().padStart(2, '0')
    val day = this.dayOfMonth.toString().padStart(2, '0')

    return "${year}-${month}-${day}"
}
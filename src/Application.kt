package com.todo

import com.apurebase.kgraphql.GraphQL
import io.ktor.application.*
import io.ktor.routing.*
import com.fasterxml.jackson.databind.*
import com.todo.route.todos
import com.todo.schema.todoSchema
import io.ktor.jackson.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.response.*
import org.jetbrains.exposed.sql.Database
import java.net.URI

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads

fun connectDB() {
    Database.connect(
        url = "jdbc:mysql://127.0.0.1/todo",
        driver = "com.mysql.cj.jdbc.Driver",
        user = "root",
        password = "root"
    )
}

fun Application.module(testing: Boolean = false) {

    connectDB()

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    install(CORS){
        val frontEnd = URI("http://localhost:8081")
        host(frontEnd.authority, schemes = listOf(frontEnd.scheme))
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        header(HttpHeaders.ContentType)
        header(HttpHeaders.AccessControlAllowOrigin)
    }

    //KGraphQL
    install(GraphQL) {
        playground = true
        schema {
            todoSchema()
        }
    }

    install(StatusPages) {
        exception<IllegalRequestFormatException> { cause ->
            val errorMessage = cause.message ?: "Unknown error"
            call.respond(HttpStatusCode.BadRequest, JsonErrorResponse(2, errorMessage))
        }
        exception<ToDoGetException> { cause ->
            val errorMessage = cause.message ?: "Unknown error"
            call.respond(HttpStatusCode.InternalServerError, JsonErrorResponse(3, errorMessage))
        }
        exception<ToDoRegisterException> { cause ->
            val errorMessage = cause.message ?: "Unknown error"
            call.respond(HttpStatusCode.InternalServerError, JsonErrorResponse(4, errorMessage))
        }
        exception<ToDoUpdateException> { cause ->
            val errorMessage = cause.message ?: "Unknown error"
            call.respond(HttpStatusCode.InternalServerError, JsonErrorResponse(5, errorMessage))
        }
        exception<ToDoDeleteException> { cause ->
            val errorMessage = cause.message ?: "Unknown error"
            call.respond(HttpStatusCode.InternalServerError, JsonErrorResponse(6, errorMessage))
        }
        exception<Exception> {
            call.respond(HttpStatusCode.InternalServerError, JsonErrorResponse(1, "サーバー内で不明なエラーが発生しました"))
        }
    }

    routing {

        todos()

    }
}


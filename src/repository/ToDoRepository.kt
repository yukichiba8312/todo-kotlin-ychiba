package com.todo.repository

import com.todo.*
import com.todo.db.table.Todos
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import java.sql.SQLException


class ToDoRepository {

    val debugmode: Boolean = false

    fun createTable() {
        try {
            transaction {
                SchemaUtils.create(Todos)
            }
        } catch (e: Exception) {
            throw Exception()
        }
    }

    fun findAll(): List<ToDo> {
        try {
            return transaction {
                if (debugmode) {
                    addLogger(StdOutSqlLogger)
                }

                Todos.selectAll().map {
                    ToDo(
                        id = it[Todos.id],
                        title = it[Todos.title].toString(),
                        detail = it[Todos.detail]?.toString(),
                        date = it[Todos.date]?.toDateString()
                    )
                }
            }
        } catch (e: SQLException) {
            throw ToDoGetException()
        }
    }

    fun getSortedList(): List<ToDo> {
        val dateComparator: Comparator<ToDo> = compareBy { it.date }
        return this.findAll().sortedWith(dateComparator)
    }

    fun register(parameter: ToDoCreateParameter) {
        transaction {
            if (debugmode) {
                addLogger(StdOutSqlLogger)
            }

            //テスト時はコメントアウトする
            createTable()

            try {
                Todos.insert {
                    it[title] = parameter.title
                    it[detail] = parameter.detail
                    if(parameter.date != null) {
                        if(parameter.date.checkDateFormat()) {
                            it[date] = parameter.date.toDateTime()
                        }
                        else {
                            throw IllegalRequestFormatException()
                        }
                    }
                    it[created_at] = DateTime.now()
                    it[updated_at] = DateTime.now()
                }
            } catch (e: SQLException) {
                throw ToDoRegisterException()
            }
        }
    }

    fun update(idNo: Long, parameter: ToDoEditParameter) {
        transaction {
            if (debugmode) {
                addLogger(StdOutSqlLogger)
            }

            val result = try {
                Todos.update({ Todos.id eq idNo }) {
                    it[title] = parameter.title
                    if (parameter.detail != null) {
                        it[detail] = parameter.detail
                    }
                    if (parameter.date != null) {
                        if(parameter.date.checkDateFormat()) {
                            it[date] = parameter.date.toDateTime()
                        }
                        else {
                            throw IllegalRequestFormatException()
                        }
                    }
                    it[updated_at] = DateTime.now()
                }
            } catch (e: SQLException) {
                throw Exception()
            }

            if (result == 0) {
                throw ToDoUpdateException()
            }
        }
    }

    fun delete(idNo: Long) {
        transaction {
            if (debugmode) {
                addLogger(StdOutSqlLogger)
            }

            val result = try {
                Todos.deleteWhere { Todos.id eq idNo }
            } catch (e: Exception) {
                throw Exception()
            }

            if (result == 0) {
                throw ToDoDeleteException()
            }
        }
    }

    fun findById(idNo: Long): ToDo {
        return transaction {
            Todos.select { Todos.id eq idNo }.map {
                ToDo(
                    id = it[Todos.id],
                    title = it[Todos.title],
                    detail = it[Todos.detail],
                    date = it[Todos.date]?.toDateString()
                )
            }.firstOrNull() ?: throw ToDoGetException()
        }
    }

    fun findByTitle(title: String): List<ToDo> {
        val list = mutableListOf<ToDo>()

        transaction {
            Todos.select { Todos.title eq title }.forEach {
                list.add(
                    ToDo(
                        id = it[Todos.id],
                        title = it[Todos.title],
                        detail = it[Todos.detail],
                        date = it[Todos.date]?.toDateString()
                    )
                )
            }
        }
        return list
    }

    fun findByDetail(detail: String?): List<ToDo> {
        val list = mutableListOf<ToDo>()

        transaction {
            Todos.select { Todos.detail eq detail }.forEach {
                list.add(
                    ToDo(
                        id = it[Todos.id],
                        title = it[Todos.title],
                        detail = it[Todos.detail],
                        date = it[Todos.date]?.toDateString()
                    )
                )
            }
        }
        return list
    }

    fun findByDate(date: String?): List<ToDo> {
        val list = mutableListOf<ToDo>()
        transaction {
            Todos.select { Todos.date eq date?.toDateTime() }.forEach {
                list.add(
                    ToDo(
                        id = it[Todos.id],
                        title = it[Todos.title],
                        detail = it[Todos.detail],
                        date = it[Todos.date]?.toDateString()
                    )
                )
            }
        }
        return list
    }

    fun findToDoFromParameters(parameter: ToDoCreateParameter): ToDo {
        return transaction {
            Todos.select { (Todos.title eq parameter.title) and
                    (Todos.detail eq parameter.detail) and
                    (Todos.date eq parameter.date?.toDateTime()) }.map {
                        ToDo(
                            id = it[Todos.id],
                            title = it[Todos.title],
                            detail = it[Todos.detail],
                            date = it[Todos.date]?.toDateString()
                        )
            }.firstOrNull() ?: throw ToDoRegisterException()
        }
    }

    fun registerFromGraphQL(parameter: ToDoCreateParameter): ToDo {
        this.register(parameter)
        return this.findToDoFromParameters(parameter)
    }

    fun updateFromGraphQL(id: Long, parameter: ToDoEditParameter): ToDo {
        this.update(id, parameter)
        return this.findById(id)
    }

    fun deleteFromGraphQL(id: Long) : String {
        this.delete(id)
        return "id = ${id} のToDoを削除しました"
    }
}
package com.todo

class IllegalRequestFormatException(): Exception("リクエストの形式が不正です")
class ToDoGetException(): Exception("一覧の取得に失敗しました")
class ToDoRegisterException(): Exception("登録に失敗しました")
class ToDoUpdateException(): Exception("更新に失敗しました")
class ToDoDeleteException(): Exception("削除に失敗しました")
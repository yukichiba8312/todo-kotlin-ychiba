package com.todo

data class JsonErrorResponse(
    val error_code: Int,
    val error_message: String
)

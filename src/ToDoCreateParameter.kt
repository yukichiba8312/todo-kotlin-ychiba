package com.todo

data class ToDoCreateParameter(
    val title: String,
    val detail: String?,
    val date: String?
)

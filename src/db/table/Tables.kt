package com.todo.db.table

import org.jetbrains.exposed.sql.Table

object Todos: Table("todos") {
    val id = long("id").autoIncrement().primaryKey()
    val title = varchar("title", 100)
    val detail = varchar("detail", 1000).nullable()
    val date = date("date").nullable()
    val created_at = datetime("created_at")
    val updated_at = datetime("updated_at")
}
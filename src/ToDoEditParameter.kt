package com.todo

data class ToDoEditParameter(
    val title: String,
    val detail: String?,
    val date: String?
)

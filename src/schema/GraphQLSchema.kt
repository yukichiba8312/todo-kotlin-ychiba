package com.todo.schema

import com.apurebase.kgraphql.schema.dsl.SchemaBuilder
import com.todo.*
import com.todo.repository.ToDoRepository

fun SchemaBuilder.todoSchema() {
    val todoRepository = ToDoRepository()
    type<ToDo>()

    connectDB()

    //Query: 全ToDoの表示
    query("allToDos") {
        resolver { -> todoRepository.findAll()
        }
    }

    //Query: IDから特定のToDo取得
    query("findById") {
        resolver { id: Int -> todoRepository.findById(id.toLong())
        }
    }

    //Query: titleからToDo取得
    query("findByTitle") {
        resolver { title: String ->
            val txt = String(title.toByteArray(), charset("UTF-8"))
            println(txt)
            todoRepository.findByTitle(title)
        }
    }

    //Query: detailからToDo取得
    query("findByDetail") {
        resolver { detail: String? -> todoRepository.findByDetail(detail)
        }
    }

    //Query: dateからToDo取得
    query("findByDate") {
        resolver { date: String? -> todoRepository.findByDate(date)
        }
    }

    //Mutation: ToDo作成
    mutation("createToDo") {
        resolver { todoData: ToDoCreateParameter  -> todoRepository.registerFromGraphQL(todoData)
        }
    }

    //Mutation: ToDo更新
    mutation("editToDo") {
        resolver { id: Int, todoData: ToDoEditParameter -> todoRepository.updateFromGraphQL(id.toLong(), todoData)
        }
    }

    //Mutation: ToDo削除
    mutation("deleteToDo") {
        resolver { id: Int -> todoRepository.deleteFromGraphQL(id.toLong())
        }
    }
}
package com.todo.route

import com.todo.*
import com.todo.repository.ToDoRepository
import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.utils.io.errors.*

fun Route.todos() {
    val todoRepository = ToDoRepository()

    route("/todos") {
        get {
            val list: List<ToDo> = todoRepository.getSortedList()
            val res = GetResponseData(JsonErrorResponse(0, ""), list)
            call.respond(arrayOf(res.jsonData, res.todoList))
        }

        post {
            val parameter = try{
                call.receive<ToDoCreateParameter>()
            } catch (e: Exception) {
                when(e) {
                    is IOException -> {
                        throw IllegalRequestFormatException()
                    }
                    else -> throw ToDoRegisterException()
                }
            }
            todoRepository.register(parameter)
            call.respond(JsonErrorResponse(0, ""))
        }

        put("/{id}") {
            val id = call.parameters["id"]?.toLong() ?: return@put
            val parameter = try {
                call.receive<ToDoEditParameter>()
            } catch (e: Exception) {
                when(e) {
                    is IOException -> {
                        throw IllegalRequestFormatException()
                    }
                    else -> throw Exception()
                }
            }
            todoRepository.update(id, parameter)
            call.respond(JsonErrorResponse(0, ""))
        }

        delete("/{id}") {
            val id = call.parameters["id"]?.toLong() ?: return@delete
            todoRepository.delete(id)
            call.respond(JsonErrorResponse(0, ""))
        }
    }
}
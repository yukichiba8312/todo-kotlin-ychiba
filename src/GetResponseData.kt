package com.todo

data class GetResponseData(
    val jsonData: JsonErrorResponse,
    val todoList: List<ToDo>
)
